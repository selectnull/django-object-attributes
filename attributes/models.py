from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.generic import GenericForeignKey
from django.utils.translation import ugettext_lazy as _


class Kind(models.Model):
    DATA_TYPES = (
        ('string', 'Text'),
        ('bool', 'Boolean'),
        ('integer', 'Integer number'),
        ('decimal', 'Decimal number'),
    )

    code = models.CharField(max_length=100,
        verbose_name=_(u'Code'))
    name = models.CharField(max_length=100,
        verbose_name=_(u'Name'))
    data_type = models.CharField(max_length=20, choices=DATA_TYPES,
        verbose_name=_(u'Data type'))

    position = models.IntegerField(null=True, blank=True,
        verbose_name=_('Position'))

    class Meta:
        db_table = "attributes_kinds"
        verbose_name = _(u"Kind")
        verbose_name_plural = _(u"Kinds")
        ordering = ('position', )

    def __unicode__(self):
        return self.name

class Attribute(models.Model):
    # content object fields
    content_type = models.ForeignKey(ContentType,
        related_name='content_type_set_for_%(class)s',
        verbose_name=_('content_type')
    )
    object_id = models.IntegerField(verbose_name=_('Object ID'))
    content_object = GenericForeignKey(
        ct_field="content_type", fk_field="object_id")

    # attribute fields
    kind = models.ForeignKey(Kind, related_name='attributes',
        verbose_name=_(u'Kind'))
    value = models.TextField(verbose_name=_(u'Value'))

    enabled = models.BooleanField(default=True, verbose_name=_(u'Enabled'))
    position = models.IntegerField(null=True, blank=True, verbose_name=_('Position'))

    class Meta:
        db_table = "attributes"
        verbose_name = _(u"Attribute")
        verbose_name_plural = _(u"Attributes")
        ordering = ('position', )

    def __unicode__(self):
        return u'{}: {}'.format(self.kind, self.value)

    def save(self, *args, **kwargs):
        if self.position is None:
            self.position = self.kind.position
        super(Attribute, self).save(*args, **kwargs)
