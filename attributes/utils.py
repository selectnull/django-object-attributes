from django.contrib import admin
from .admin import AttributeInline


def add_inline(model, model_admin):
    admin.site.unregister(model)
    model_admin.inlines += (AttributeInline, )
    admin.site.register(model, model_admin)
