from django import template
from django.contrib.contenttypes.models import ContentType
from ..models import Attribute

from collections import defaultdict


register = template.Library()


class AttributesNode(template.Node):
    def __init__(self, obj, output_name):
        self.obj = template.Variable(obj)
        self.output_name = output_name

    def render(self, context):
        obj = self.obj.resolve(context)
        content_type = ContentType.objects.get_for_model(obj)
        data = Attribute.objects.select_related().filter(
            object_id=obj.pk, content_type=content_type, enabled=True).order_by('kind', 'position')
        result = defaultdict(list)
        for x in data:
            result[x.kind.code].append(x.value)
            
        context[self.output_name] = result
        return ''

@register.tag
def get_attributes(parser, token):
    """
    {% get_attributes obj as output_name %}

    """
    # bits = token.contents.split()
    # tag_name = bits[0]
    try:
        tag_name, obj, _as, output_name = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError('{} tag takes exactly 3 arguments'.format(tag_name))
    return AttributesNode(obj, output_name)
