from django.contrib import admin
from django.contrib import contenttypes
from .models import Attribute, Kind


class KindAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'data_type', 'position', )
    search_fields = ('code', 'name', )
    list_filter = ('data_type', )

class AttributeInline(contenttypes.generic.GenericStackedInline):
    model = Attribute
    extra = 0
    ct_field = 'content_type'
    fk_field = 'object_id'

    fieldsets = (
        (None, {
            'fields': (('kind', 'position', 'enabled'), ('value', ), ),
        }),
    )

admin.site.register(Kind, KindAdmin)
